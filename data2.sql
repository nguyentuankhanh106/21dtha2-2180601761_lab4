USE [master]
GO
/****** Object:  Database [QuanLySV]    Script Date: 10/20/2023 8:20:35 AM ******/
CREATE DATABASE [QuanLySV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QuanLySV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MAY1\MSSQL\DATA\QuanLySV.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'QuanLySV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.MAY1\MSSQL\DATA\QuanLySV_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [QuanLySV] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuanLySV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuanLySV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuanLySV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuanLySV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuanLySV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuanLySV] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuanLySV] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuanLySV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuanLySV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuanLySV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuanLySV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuanLySV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuanLySV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuanLySV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuanLySV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuanLySV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QuanLySV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuanLySV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuanLySV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuanLySV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuanLySV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuanLySV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuanLySV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuanLySV] SET RECOVERY FULL 
GO
ALTER DATABASE [QuanLySV] SET  MULTI_USER 
GO
ALTER DATABASE [QuanLySV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuanLySV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuanLySV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuanLySV] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [QuanLySV] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [QuanLySV] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'QuanLySV', N'ON'
GO
ALTER DATABASE [QuanLySV] SET QUERY_STORE = ON
GO
ALTER DATABASE [QuanLySV] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [QuanLySV]
GO
/****** Object:  Table [dbo].[Faculty]    Script Date: 10/20/2023 8:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Faculty](
	[FacultyID] [int] IDENTITY(1,1) NOT NULL,
	[FacultyName] [nvarchar](200) NOT NULL,
	[TotalProfessor] [int] NULL,
 CONSTRAINT [PK_Faculty] PRIMARY KEY CLUSTERED 
(
	[FacultyID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student]    Script Date: 10/20/2023 8:20:36 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student](
	[StudentID] [nvarchar](20) NOT NULL,
	[FullName] [nvarchar](200) NOT NULL,
	[AverageScore] [float] NOT NULL,
	[FacultyID] [int] NOT NULL,
 CONSTRAINT [PK_Student] PRIMARY KEY CLUSTERED 
(
	[StudentID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Faculty] ON 

INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (1, N'Công Nghệ Thông Tin', NULL)
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (2, N'Ngôn Ngữ Anh', NULL)
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (3, N'Quản Trị Kinh Doanh', NULL)
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (7, N'Công Nghệ Ô Tô', 4)
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (8, N'Công Nghệ Sinh Học', 5)
INSERT [dbo].[Faculty] ([FacultyID], [FacultyName], [TotalProfessor]) VALUES (10, N'Công Nghệ Phần Mềm', 2)
SET IDENTITY_INSERT [dbo].[Faculty] OFF
GO
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'1000', N'Hoang', 5, 8)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2002', N'Le Van', 5.6, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605111', N'Nguyễn Văn Bé 1', 0.7057133, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605112', N'Nguyễn Văn Bé 2', 4.606979268, 2)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605113', N'Nguyễn Văn Bé 3', 7.731940883, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605114', N'Nguyễn Văn Bé 4', 2.617283438, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605115', N'Nguyễn Văn Bé 5', 6.900510072, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605116', N'Nguyễn Văn Bé 6', 9.440960444, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605117', N'Nguyễn Văn Bé 7', 9.753125811, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605118', N'Nguyễn Văn Bé 8', 5.086110671, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605119', N'Nguyễn Văn Bé 9', 0.364689669, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605120', N'Nguyễn Văn Bé 10', 0.509553929, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605121', N'Nguyễn Văn Bé 11', 9.054008828, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605122', N'Nguyễn Văn Bé 12', 5.912143501, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605123', N'Nguyễn Văn Bé 13', 0.507897054, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605124', N'Nguyễn Văn Bé 14', 7.39516869, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605125', N'Nguyễn Văn Bé 15', 1.154631884, 2)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605126', N'Nguyễn Văn Bé 16', 0.769977784, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605127', N'Nguyễn Văn Bé 17', 8.880945133, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605128', N'Nguyễn Văn Bé 18', 5.986979188, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605129', N'Nguyễn Văn Bé 19', 0.878634071, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605130', N'Nguyễn Văn Bé 20', 5.250484971, 2)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605131', N'Nguyễn Văn Bé 21', 1.949493948, 2)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605132', N'Nguyễn Văn Bé 22', 3.92890935, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605133', N'Nguyễn Văn Bé 23', 4.214892848, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605134', N'Nguyễn Văn Bé 24', 0.074356708, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605135', N'Nguyễn Văn Bé 25', 0.722116639, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605136', N'Nguyễn Văn Bé 26', 0.475435109, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605137', N'Nguyễn Văn Bé 27', 9.957889835, 2)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605138', N'Nguyễn Văn Bé 28', 0.715444644, 3)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605139', N'Nguyễn Văn Bé 29', 1.62225649, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'2180605140', N'Nguyễn Văn Bé 30', 5.576552292, 1)
INSERT [dbo].[Student] ([StudentID], [FullName], [AverageScore], [FacultyID]) VALUES (N'218060521', N'Tri Vo', 6.5, 1)
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_Student_Faculty] FOREIGN KEY([FacultyID])
REFERENCES [dbo].[Faculty] ([FacultyID])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_Student_Faculty]
GO
USE [master]
GO
ALTER DATABASE [QuanLySV] SET  READ_WRITE 
GO
